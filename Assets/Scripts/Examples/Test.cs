using UnityEngine;

namespace Examples
{
    public class Test : MonoBehaviour
    {
        public int FieldA;
        public string FieldAStr;
        public int FieldB;
        public string FieldBStr;
        public Vector3 FieldCV3;
    }
}
