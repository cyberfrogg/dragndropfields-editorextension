using UnityEngine;

namespace Examples
{
    public class TestAnother : MonoBehaviour
    {
        public int AnotherA;
        public string AnotherAStr;
        public int AnotherB;
        public string AnotherBStr;
        public Vector3 AnotherCV3;
    }
}
