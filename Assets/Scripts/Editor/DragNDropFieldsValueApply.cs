﻿using UnityEditor;
using UnityEngine;

namespace Editor
{
    public static class DragNDropFieldsValueApply
    {
        public static void Apply(SerializedProperty property, object value)
        {
            switch (property.propertyType)
           {
             case SerializedPropertyType.Generic:
               Debug.LogWarning("Get/Set of Generic SerializedProperty not supported");
               break;
             case SerializedPropertyType.Integer:
               property.intValue = (int) value;
               break;
             case SerializedPropertyType.Boolean:
               property.boolValue = (bool) value;
               break;
             case SerializedPropertyType.Float:
               property.floatValue = (float) value;
               break;
             case SerializedPropertyType.String:
               property.stringValue = (string) value;
               break;
             case SerializedPropertyType.Color:
               property.colorValue = (Color) value;
               break;
             case SerializedPropertyType.ObjectReference:
               property.objectReferenceValue = value as UnityEngine.Object;
               break;
             case SerializedPropertyType.LayerMask:
               property.intValue = (int) value;
               break;
             case SerializedPropertyType.Enum:
               property.enumValueIndex = (int) value;
               break;
             case SerializedPropertyType.Vector2:
               property.vector2Value = (Vector2) value;
               break;
             case SerializedPropertyType.Vector3:
               property.vector3Value = (Vector3) value;
               break;
             case SerializedPropertyType.Vector4:
               property.vector4Value = (Vector4) value;
               break;
             case SerializedPropertyType.Rect:
               property.rectValue = (Rect) value;
               break;
             case SerializedPropertyType.ArraySize:
               property.intValue = (int) value;
               break;
             case SerializedPropertyType.Character:
               property.stringValue = (string) value;
               break;
             case SerializedPropertyType.AnimationCurve:
               property.animationCurveValue = value as AnimationCurve;
               break;
             case SerializedPropertyType.Bounds:
               property.boundsValue = (Bounds) value;
               break;
             case SerializedPropertyType.Gradient:
               Debug.LogWarning("Get/Set of Gradient SerializedProperty not supported");
               break;
             case SerializedPropertyType.Quaternion:
               property.quaternionValue = (Quaternion) value;
               break;
           }
        }
    }
}