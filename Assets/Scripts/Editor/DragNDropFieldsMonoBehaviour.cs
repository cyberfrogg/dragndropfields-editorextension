using System;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(MonoBehaviour), true)]
    public class DragNDropFieldsMonoBehaviour : UnityEditor.Editor
    {
        private const int DRAG_MOUSE_BUTTON = 2;

        private DragNDropFieldPropertyData _dragData; 
        
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            var property = serializedObject.GetIterator();
            if (property.NextVisible(true)) {
                do
                {
                    OnFieldDraw(property, Event.current);
                }
                while (property.NextVisible(false));
            }
        }

        private void OnFieldDraw(SerializedProperty property, Event e)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty(property.name), true);
            serializedObject.ApplyModifiedProperties();
            
            var lastRect = GUILayoutUtility.GetLastRect();
            
            var isMouseInRect = lastRect.Contains(e.mousePosition);

            if (isMouseInRect)
            {
                EditorGUI.DrawRect(lastRect, new Color(0f, 0f, 0f, 0.1f));
                
                if (e.type == EventType.MouseDown && e.button == DRAG_MOUSE_BUTTON)
                {
                    var value = ExtractValueFromProperty(property);
                    
                    _dragData = new DragNDropFieldPropertyData(property.displayName, property.name, property.propertyPath, value);
                    
                    Debug.Log($"Picking value: {_dragData.Value}");
                }
                
                if (Event.current.type == EventType.MouseUp && Event.current.button == DRAG_MOUSE_BUTTON && _dragData != null)
                {
                    Debug.Log($"Dropping value: {_dragData.Value}");
                    DragNDropFieldsValueApply.Apply(property, _dragData.Value);
                    _dragData = null;
                }
            }
        }

        private object ExtractValueFromProperty(SerializedProperty property)
        {
            var targetObject  = property.serializedObject.targetObject;
            var targetObjectClassType = targetObject.GetType();
            var field = targetObjectClassType.GetField(property.propertyPath);

            if (field != null)
            {
                var value = field.GetValue(targetObject);
                return value;
            }

            return null;
        }
    }
}