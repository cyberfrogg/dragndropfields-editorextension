﻿using System;

namespace Editor
{
    [Serializable]
    public class DragNDropFieldPropertyData
    {
        public string DisplayName;
        public string Name;
        public string Path;
        public object Value;

        public DragNDropFieldPropertyData(string displayName, string name, string path, object value)
        {
            DisplayName = displayName;
            Name = name;
            Path = path;
            Value = value;
        }

        public DragNDropFieldPropertyData(object value)
        {
            DisplayName = "No Name";
            Name = "";
            Path = "";
            Value = value;
        }

        public override string ToString()
        {
            return $"DragNDropFieldPropertyData: \n" +
                   $"DisplayName: {DisplayName} \n" +
                   $"Name: {Name} \n" +
                   $"Path: {Path} \n" +
                   $"ValueType: {Value}";
        }
    }
}